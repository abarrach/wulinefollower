#include "testApp.h"

using namespace ofxCv;
using namespace cv;

// sort BlobInfo by Y position
bool sort_fun (const ofPoint& x,const ofPoint& y) {
    return x.x < y.x;
}
void testApp::setup() {

    if( XML.loadFile("wuLineFollowersSettings.xml") ){
		cout << "wuLineFollowersSettings.xmll loaded!" << endl;
	}else{
		cout << "unable to load wuLineFollowersSettings.xml check data/ folder" << endl;
	}
    
	//read the colors from XML
	//if the settings file doesn't exist we assigns default values 15
	thresX		= XML.getValue("THRESHOLD:CANNY:X", 15);
	thresY		= XML.getValue("THRESHOLD:CANNY:Y", 15);

    camNum = XML.getValue("CAM:NUM", 1);
    
	/*
     "BACKGROUND:COLOR:RED" referes to a structure like this:
     
     <BACKGROUND>
     <COLOR>
     <RED>101.103516</RED>
     </COLOR>
     </BACKGROUND>
     */

    cam.setDeviceID(camNum);
	cam.initGrabber(640, 480);
    
    threshold = 15;

    contourFinder.setMinAreaRadius(10);
	contourFinder.setMaxAreaRadius(100);
	contourFinder.setThreshold(threshold);
    contourFinder.setSimplify(true);
    linePos=0;
    lastTime=0;

    edge.allocate(640, 480, OF_IMAGE_GRAYSCALE);
    colorImg.allocate(640, 480, OF_IMAGE_COLOR);

    mCurStep = 1;
    mSenderAddr = "127.0.0.1";   // Needs to be set to the server's address (using config file)
    mSenderPort = 57120;            // Default SuperCollider language port
    mReceiverPort = 13580;          // Port we receive on; needs to match the SC

    // Setup OSC communications
    mSender.setup(mSenderAddr, mSenderPort);
    ofLog() << "Using server: '" << mSenderAddr << "' on port " << mSenderPort;
    mReceiver.setup(mReceiverPort);
    ofLog() << "Receiving on port: " << mReceiverPort;

    bOnline = true;
    bCapture = false;

    for(int i=0; i<MAXLINES; i++)
        lineIds[i]=false;

}

void testApp::update() {
	cam.update();
	if(cam.isFrameNew()) {
        colorImg.setFromPixels(cam.getPixels(), 640, 480, OF_IMAGE_COLOR);
		convertColor(cam, gray, CV_RGB2GRAY);
		Canny(gray, edge, thresX, thresY, 3);
		edge.update();

    }

    if(listenOsc() || bCapture )
    {
        bCapture = false;

        multilineStruct &step = linesAtStep[mCurStep-1];
        for(int i=0; i< step.lines.size(); i++)
            lineIds[step.lines[i].iden]=false;

        step.lines.clear();

        blur(edge, 3);
        contourFinder.findContours(edge);
        int step_max = mCurStep * 640/MAXSTEPS;
        int step_min = (mCurStep * 640/MAXSTEPS) - 640/MAXSTEPS;

        for(int k=0; k< contourFinder.size(); k++)
        {
            poline = contourFinder.getPolyline(k);
            poline= poline.getResampledBySpacing(1);
            ofRectangle rect;
            rect.x = contourFinder.getBoundingRect(k).x;
            rect.y = contourFinder.getBoundingRect(k).y;
            rect.width = contourFinder.getBoundingRect(k).width;
            rect.height = contourFinder.getBoundingRect(k).height;

            if (rect.x > step_min && rect.x < step_max && rect.x+rect.width > 70 && rect.y+rect.height > 70 && rect.y<430)// && poline.getArea()>10)
            {
                lineTmp.pts.clear();
                lineTmp.pos=0;

                int min_x=2000;
                int firstElem = 0;

                for(int i=0; i< poline.size(); i++) {
                    if(poline[i].x<min_x)
                    {
                        firstElem = i;
                        min_x = poline[i].x;
                    }
                }
                for(int i=firstElem; i< poline.size(); i++) {
                    bool bIsolate=true;

                    for(int j=0; j< lineTmp.pts.size(); j++) {
                        if( (poline[i].x - lineTmp.pts[j].x)*(poline[i].x - lineTmp.pts[j].x) + (poline[i].y-lineTmp.pts[j].y)*(poline[i].y-lineTmp.pts[j].y) < 400)
                            bIsolate=false;
                    }
                    if(bIsolate)
                    {
                        ofPoint point;
                        point.x = poline[i].x;
                        point.y = poline[i].y;

                        lineTmp.pts.push_back(point);
                    }

                }
                for(int i=0; i< firstElem; i++) {
                    bool bIsolate=true;

                    for(int j=0; j< lineTmp.pts.size(); j++) {
                        if( (poline[i].x - lineTmp.pts[j].x)*(poline[i].x - lineTmp.pts[j].x) + (poline[i].y-lineTmp.pts[j].y)*(poline[i].y-lineTmp.pts[j].y) < 400)
                            bIsolate=false;
                    }
                    if(bIsolate)
                    {
                        ofPoint point;
                        point.x = poline[i].x;
                        point.y = poline[i].y;
                        lineTmp.pts.push_back(point);
                    }

                }

                lineTmp.dead=false;
                lineTmp.loop_pt = -1;
                lineTmp.pos=-1;

                // find loop point
                int last = lineTmp.pts.size()-1;
                if(last>8)
                {
                    for(int i=last-5; i>0 ; i--)
                    {
                        if( (lineTmp.pts[i].x-lineTmp.pts[last].x)*(lineTmp.pts[i].x-lineTmp.pts[last].x) + (lineTmp.pts[i].y-lineTmp.pts[last].y)*(lineTmp.pts[i].y-lineTmp.pts[last].y) < 2000)
                        {
                            lineTmp.loop_pt=i;
                        }
                    }
                }

                //find ID
                bool bTaken=false;
                for(int i=0; i<MAXLINES; i++)
                {
                    if(lineIds[i]==false)
                    {
                        lineTmp.iden=i;
                        lineIds[i]=true;
                        bTaken=true;
                        i=MAXLINES;
                    }
                }
                if(bTaken)
                    step.lines.push_back(lineTmp);
                else
                    cout << "too much lines" << endl;

            }
        }
    }

    if(ofGetElapsedTimeMillis() - lastTime > 110)
    {
        lastTime = ofGetElapsedTimeMillis();
        for(int j=0; j< MAXSTEPS; j++) {
            for(int i=0; i< linesAtStep[j].lines.size(); i++) {
                if(linesAtStep[j].lines[i].dead==false)
                {
                    linesAtStep[j].lines[i].pos++;
                    if(linesAtStep[j].lines[i].pos>linesAtStep[j].lines[i].pts.size()-1)
                    {
                        // NO LOOPS
//                        if(linesAtStep[j].lines[i].loop_pt<0)
//                        {
                            linesAtStep[j].lines[i].dead=true;
                            sendOscLineEnd(linesAtStep[j].lines[i].iden);
//                        }
//                        else
//                        {
//                            linesAtStep[j].lines[i].pos=linesAtStep[j].lines[i].loop_pt;
//                            sendOscLine(linesAtStep[j].lines[i].iden, linesAtStep[j].lines[i].pts[linesAtStep[j].lines[i].pos].y);
 //                       }
                    }else
                        sendOscLine(linesAtStep[j].lines[i].iden, linesAtStep[j].lines[i].pts[linesAtStep[j].lines[i].pos].y);

                }
            }
        }
    }

}

void testApp::draw() {
    ofSetColor(255,255,255);
	colorImg.draw(0, 0);
	edge.draw(640, 0);

    ofNoFill();
    ofSetColor(0,0,255);
/*    for(int k=0; k< contourFinder.size(); k++)
    {
        ofRectangle rect;
        rect.x = contourFinder.getBoundingRect(k).x;
        rect.y = contourFinder.getBoundingRect(k).y;
        rect.width = contourFinder.getBoundingRect(k).width;
        rect.height = contourFinder.getBoundingRect(k).height;
        ofRect(rect);
    }
*/
    for(int j=0; j< MAXSTEPS; j++) {
        for(int i=0; i< linesAtStep[j].lines.size(); i++) {
            for(int k=0; k< linesAtStep[j].lines[i].pts.size(); k++)
                ofCircle(linesAtStep[j].lines[i].pts[k].x, linesAtStep[j].lines[i].pts[k].y, 6);
        }
    }

    ofFill();
    ofSetColor(255,0,0);
    for(int j=0; j< MAXSTEPS; j++) {
        for(int i=0; i< linesAtStep[j].lines.size(); i++) {
            if(linesAtStep[j].lines[i].dead==false)
                ofCircle(linesAtStep[j].lines[i].pts[linesAtStep[j].lines[i].pos].x, linesAtStep[j].lines[i].pts[linesAtStep[j].lines[i].pos].y, 10);
        }
    }

    ofEnableAlphaBlending();
    for(int i=1; i < MAXSTEPS+1; i++)
    {
        int step_max = i * 640/MAXSTEPS;
        int step_min = (i * 640/MAXSTEPS) - 640/MAXSTEPS;
        ofSetColor(192,192,192,60);
        ofLine(step_max, 0, step_max, 480);
    }
    
    for(int i=1; i < MAXSCALENOTES; i++)
    {
        int note_mark = (i * 480/MAXSCALENOTES);
        ofSetColor(192,192,192,90);
        ofLine(0, note_mark, 640, note_mark);
    }
    if(mCurStep>0)
    {
        ofSetColor(255, 0, 0, 40);
		ofFill();
        ofRect((mCurStep-1)* (640/MAXSTEPS), 0, (640/MAXSTEPS), 480);
    }
    ofDisableAlphaBlending();


}


//--------------------------------------------------------------
void testApp::keyPressed(int key)
{


	if(key == '1')
    {
        mCurStep = 1;
        bCapture=true;
    }
	if(key == '2')
    {
        mCurStep = 2;
        bCapture=true;
    }
	if(key == '3')
    {
        mCurStep = 3;
        bCapture=true;
    }
	if(key == '4')
    {
        mCurStep = 4;
        bCapture=true;
    }
	if(key == '5')
    {
        mCurStep = 5;
        bCapture=true;
    }
    if(key == OF_KEY_UP)
    {
        threshold++;
       	contourFinder.setThreshold(threshold);
    }
    if(key == OF_KEY_DOWN)
    {
        threshold--;
       	contourFinder.setThreshold(threshold);
    }
}

bool testApp::listenOsc()
{
    bool bNewMsg = false;
	// check for waiting messages
	while(mReceiver.hasWaitingMessages()){
		// get the next message
		ofxOscMessage m;
		mReceiver.getNextMessage(&m);

        if (bOnline) {
            // check for mouse moved message
            if(m.getAddress() == "/server/step"){
                mCurStep = m.getArgAsInt32(0) + 1;
                if(mCurStep > MAXSTEPS)
                    mCurStep = 0;
            }

            bNewMsg = true;
        }
    }

	return bNewMsg;
}

void testApp::sendOscLine(int _iden, int _y)
{

    ofxOscMessage msg;
    msg.setAddress("/line");

    msg.addIntArg(_iden);
    msg.addFloatArg(((480.0-_y)/480)*MAXSCALENOTES);
    mSender.sendMessage(msg);

}

void testApp::sendOscLineEnd(int _iden)
{
    ofxOscMessage msg;
    msg.setAddress("/lineEnd");

    msg.addIntArg(_iden);
    mSender.sendMessage(msg);

}


//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

    thresX=x;
    thresY=y;
    
	//update the colors to the XML structure when the mouse is released
	XML.setValue("THRESHOLD:CANNY:X", thresX);
	XML.setValue("THRESHOLD:CANNY:Y", thresY);
    XML.saveFile("wuLineFollowersSettings.xml");
}

