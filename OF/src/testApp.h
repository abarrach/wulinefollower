#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "ofxOpenCv.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"

#define MAXSTEPS 16
#define MAXLINES 20
#define MAXSCALENOTES 8


struct lineStruct {
    vector<ofPoint> pts;
    int pos;
    int loop_pt;
    bool dead;
    int iden;
};

struct multilineStruct {
    vector<lineStruct>  lines;
};

class testApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
    void mousePressed(int x, int y, int button);
	void keyPressed  (int key);

    bool listenOsc();
    void sendOscLine(int iden, int y);
    void sendOscLineEnd(int iden);

	ofVideoGrabber cam;
	ofPixels gray;
    ofImage colorImg;
	ofImage edge;
	ofxCv::ContourFinder contourFinder;
    vector<cv::Point> * contourPoints;

    int threshold;

    // array de 16 steps, en cada un podem tenir lines, que son vectors de punts
    multilineStruct  linesAtStep[MAXSTEPS];
    lineStruct lineTmp;

    ofPolyline poline;
    int linePos;
    unsigned long lastTime;
    bool lineIds[MAXLINES];

    // steps
    int                         mCurStep;
    bool bCapture, bOnline;

    // Config / OSC stuff
    ofxOscSender                mSender;
    ofxOscReceiver              mReceiver;
    string                      mSenderAddr;
    int                         mSenderPort;
    int                         mReceiverPort;

    // XML
    ofxXmlSettings XML;
    int thresX, thresY;
    int camNum;

};
